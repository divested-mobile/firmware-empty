LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_DEVICE),tangorpro)

AB_OTA_PARTITIONS += \
	abl \
	bl1 \
	bl2 \
	bl31 \
	gsa \
	ldfw \
	pbl \
	tzsw

include vendor/firmware/tangorpro/AndroidBoardVendor.mk

endif
